******************
Portal da Codevasf
******************

.. contents:: Conteúdos
   :depth: 2

Introdução
==========

Este pacote contem o código fonte do Portal da Codevasf.
A documentação inclusa descreve o procedimento de instalação e atualização baseado na Identidade Digital do Governo (IDG).

Esta documentação não pretende substituir a `documentação do IDG <http://identidade-digital-de-governo-plone.readthedocs.io/>`_,
mas complementar ela adicionando só os passos necessários para instalar os componentes do portal.

Componentes do portal
=====================

Este pacote inclui as seguintes customizações e componentes:

* Tema padrão da Codevasf

Pré-requisitos
==============

A instalação do portal requere de uma série de pré-requisitos de software e hardware que devem ser satisfeitos.
Os pré-requisitos estão descritos a fundo na documentação do IDG:

Software
--------

* Sistema operacional homologado (Debian/Ubuntu ou CentOS) atualizado na versão suportada mais recente
* Dependências de compilação do Zope instaladas e atualizadas
* Ambiente virtual de Python 2.7 atualizado
* Usuário ``plone`` criado

Consulte a documentação do IDG para maior informação sobre como instalar cada um desses componentes.

Hardware
--------

Os requerimentos de hardware dependem do número de acessos de usuários anónimos e número de editores simultáneos.
Tomando em conta o histórico do portal sugerimos a instalação em máquinas virtuais com pelo menos um processador e 4GB de memória por instância de Zope ativa.

O número total de máquinas virtuais necessárias não pode ser calculado com antecedencia,
pois o valor dependerá de parámetros de configuração de caching e do uso do portal.

Vários problemas de software conhecidos no IDG que causavam consumo alto de recursos tem sido resolvidos nos últimos anos,
pelo que consideramos que o portal deveria ter um funcionamento estável con recursos bem inferiores aos atuais.

Porém, recomendamos iniciar com uma arquitetura similar e se ajustar para em baixo toda vez que tenha sido confirmado que os recursos são suficientes.

Atualização
===========

.. warning::
    A instalação do portal está baseada no release 1.5.2 do IDG.
    Qualquer combinação de versões diferente da descrita neste documento não está homologada e não será suportada.

Confira que está usando a release certa:

.. code-block:: console

    $ git status
    HEAD detached at 1.5.1
    nothing to commit, working directory clean
    $ git checkout 1.5.2
    Previous HEAD position was dd5a829... Update versions for release 1.5.1
    HEAD is now at 15dd6c3... Release 1.5.2.

Confira as seguintes modificações no seu buildout de produção:

.. code-block:: ini

    [buildout]
    ...
    extensions = mr.developer
    always-checkout = force
    auto-checkout =
        codevasf.portal
        codevasf.webservice

    [sources]
    codevasf.portal = git https://bitbucket.org/forcontent/codevasf.portal.git rev=64cb836
    codevasf.webservice = git https://bitbucket.org/forcontent/codevasf.webservice.git rev=4ac8900

    [versions]
    path.py = 11.1.0



Rode o buildout:

.. code-block:: console

    $ bin/buildout

Novos componentes serão descarregados.
Caso de falhas revise o log do buildout corregindo qualquer problema detetado.

Reinicie as instâncias e fique atento aos logs de eventos das instâncias.

Acesse a "Configuração do Site".
Um aviso para atualizar a configuração deve estar sendo mostrado;
acesse o link "continue com a atualização" e selecione o botão "Atualização".
Confira a atualização da versão do Plone para 4.3.17.

Acesse novamente a "Configuração do Site" e selecione "Complementos".
Atualize as versões dos seguintes complementos: brasil.gov.agenda, brasil.gov.tiles e brasil.gov.portal.

Navegue pelo portal revisando sempre os logs de eventos das instâncias em busca de mensagens de erro.

Relate qualquer problema detetado.
