# -*- coding: utf-8 -*-
from codevasf.portal.config import PROJECTNAME
from Products.CMFPlone.interfaces import INonInstallable
from Products.CMFQuickInstallerTool import interfaces as BBB
from zope.interface import implementer

import logging


logger = logging.getLogger(PROJECTNAME)


@implementer(BBB.INonInstallable)
@implementer(INonInstallable)
class NonInstallable(object):

    @staticmethod
    def getNonInstallableProducts():
        """Hide in the add-ons configlet."""
        return [
            u'codevasf.portal.upgrades.v1001',
        ]

    @staticmethod
    def getNonInstallableProfiles():
        """Hide at site creation."""
        return [
            u'codevasf.portal.upgrades.v1001:default',
            u'codevasf.portal:uninstall',
        ]


def post_install(context):
    """Post install script."""


def post_uninstall(context):
    """Post uninstall script."""
