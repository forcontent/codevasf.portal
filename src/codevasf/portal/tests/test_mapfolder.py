# -*- coding: utf-8 -*-
from codevasf.portal.content import IMapFolder
from codevasf.portal.testing import INTEGRATION_TESTING
from plone import api
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


class MapFolderTypeTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        with api.env.adopt_roles(['Manager']):
            self.folder = api.content.create(
                self.portal, 'MapFolder', 'folder')

    def test_adding(self):
        self.assertTrue(IMapFolder.providedBy(self.folder))

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='MapFolder')
        self.assertIsNotNone(fti)

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='MapFolder')
        schema = fti.lookupSchema()
        self.assertEqual(IMapFolder, schema)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='MapFolder')
        factory = fti.factory
        new_object = createObject(factory)
        self.assertTrue(IMapFolder.providedBy(new_object))

    def test_is_selectable_as_folder_default_view(self):
        self.portal.setDefaultPage('folder')
        self.assertEqual(self.portal.default_page, 'folder')
