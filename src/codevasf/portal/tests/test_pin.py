# -*- coding: utf-8 -*-
from codevasf.portal.content import IPin
from codevasf.portal.testing import INTEGRATION_TESTING
from plone import api
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


class PinTypeTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        with api.env.adopt_roles(['Manager']):
            self.folder = api.content.create(
                self.portal, 'MapFolder', 'folder')

        self.pin = api.content.create(
            self.folder, 'Pin', 'pin')

    def test_adding(self):
        self.assertTrue(IPin.providedBy(self.pin))

    def test_add_constraint(self):
        with self.assertRaises(api.exc.InvalidParameterError):
            api.content.create(self.portal, 'Pin', 'foo')

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Pin')
        self.assertIsNotNone(fti)

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Pin')
        schema = fti.lookupSchema()
        self.assertEqual(IPin, schema)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Pin')
        factory = fti.factory
        new_object = createObject(factory)
        self.assertTrue(IPin.providedBy(new_object))

    def test_is_selectable_as_folder_default_view(self):
        self.folder.setDefaultPage('pin')
        self.assertEqual(self.folder.default_page, 'pin')

    def test_lead_image_behavior(self):
        from plone.app.contenttypes.behaviors.leadimage import ILeadImage
        self.assertTrue(ILeadImage.providedBy(self.pin))
