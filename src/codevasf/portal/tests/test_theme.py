# -*- coding: utf-8 -*-
from codevasf.portal.testing import INTEGRATION_TESTING

import unittest


class ThemeTestCase(unittest.TestCase):

    """Ensure theme is properly installed."""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']

    def test_available_themes(self):
        from plone.app.theming.utils import getAvailableThemes
        themes = getAvailableThemes()
        self.assertEqual(len(themes), 7)

    def test_theme_enabled(self):
        from plone.app.theming.utils import isThemeEnabled
        self.assertTrue(isThemeEnabled(self.request))

    def test_current_theme(self):
        from plone.app.theming.utils import getCurrentTheme
        theme = getCurrentTheme()
        self.assertEqual(theme, u'codevasf')

    def test_default_theme(self):
        from plone.app.theming.utils import getTheme
        theme = getTheme('codevasf')
        self.assertIsNotNone(theme)
        self.assertEqual(theme.title, u'Default')
        self.assertEqual(theme.description, 'Tema para Portal da Codevasf')
        self.assertEqual(theme.absolutePrefix, '/++theme++codevasf')
        self.assertEqual(theme.rules, '/++theme++codevasf/rules.xml')
        self.assertEqual(theme.doctype, '<!DOCTYPE html>')
