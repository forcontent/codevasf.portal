# -*- coding: utf-8 -*-
from codevasf.portal.config import PROJECTNAME
from codevasf.portal.interfaces import IBrowserLayer
from codevasf.portal.testing import INTEGRATION_TESTING
from plone.browserlayer.utils import registered_layers

import unittest


DEPENDENCIES = (
    'brasil.gov.portal',
)


class InstallTestCase(unittest.TestCase):
    """Ensure product is properly installed."""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.qi = self.portal['portal_quickinstaller']

    def test_installed(self):
        self.assertTrue(self.qi.isProductInstalled(PROJECTNAME))

    def test_dependencies_installed(self):
        expected = set(DEPENDENCIES)
        installed = self.qi.listInstalledProducts(showHidden=True)
        installed = set([product['id'] for product in installed])
        result = sorted(expected - installed)
        self.assertEqual(result, [], 'Not installed: ' + ', '.join(result))

    def test_browser_layer_installed(self):
        self.assertIn(IBrowserLayer, registered_layers())

    def test_add_permissions(self):
        permissions = (
            'codevasf.portal: Add Map Folder',
            'codevasf.portal: Add Pin',
        )
        expected = ['Contributor', 'Manager', 'Owner', 'Site Administrator']
        for p in permissions:
            roles = self.portal.rolesOfPermission(p)
            roles = [r['name'] for r in roles if r['selected']]
            self.assertListEqual(roles, expected)
