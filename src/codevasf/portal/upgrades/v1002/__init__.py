# -*- coding: utf-8 -*-
from codevasf.portal.upgrades import add_tile


def add_twoselects_tile(context):
    """Add Two Selects Tile."""
    add_tile(u'codevasf.portal.twoselects')
