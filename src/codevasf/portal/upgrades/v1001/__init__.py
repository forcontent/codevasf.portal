# -*- coding: utf-8 -*-
from codevasf.portal.logger import logger
from plone import api


def update_portal(setup_tool):
    """Update portal:

    - Add and enable "codevasf.portal.tabs" tile
    - Do not allow files inside pins
    """
    tile = u'codevasf.portal.tabs'

    name = 'plone.app.tiles'
    value = api.portal.get_registry_record(name=name)
    if tile not in value:
        value.append(tile)
        api.portal.set_registry_record(name=name, value=value)
        assert tile in api.portal.get_registry_record(name=name)
        logger.info('Tile registered')

    name = 'collective.cover.controlpanel.ICoverSettings.available_tiles'
    value = api.portal.get_registry_record(name=name)
    if tile not in value:
        value.append(tile)
        api.portal.set_registry_record(name=name, value=value)
        assert tile in api.portal.get_registry_record(name=name)
        logger.info('Tile available')

    portal_types = api.portal.get_tool('portal_types')
    pin = portal_types['Pin']
    pin.allowed_content_types = ()
    logger.info('Pin does not allow File inside anymore')
