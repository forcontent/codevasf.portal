# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from Acquisition import aq_parent
from codevasf.webservice.api import get_api_instance
from codevasf.portal.config import APISANDBOX

try:
    import simplejson as json
except:
    import json

from plone.memoize import view
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class ConsultProcessesView(BrowserView):
    template = ViewPageTemplateFile('templates/consult_processes.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.response = self.apiCodevasf.process_consult.list()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    @view.memoize
    def getProcesses(self):
        """ """
        fl = self.form.get('number_process')
        if fl:
            return [vl for vl in self.response.body['processos'] if
                    vl.get('id') == fl]

    def __call__(self):
        """"""
        self._setup()
        return self.template()
