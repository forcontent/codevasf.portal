# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from codevasf.webservice.api import get_api_instance
from codevasf.portal.config import APISANDBOX

try:
    import simplejson as json
except:
    import json

from plone.memoize import view
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class ConsultExercisesView(BrowserView):
    template = ViewPageTemplateFile('templates/consult_exercises.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.response = self.apiCodevasf.exercises.list()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    @view.memoize
    def getExercises(self, filter=None):
        """ """
        if not filter:
            return sorted([{"id": fl.get('id')} for fl in self.response.body['exercicios']], reverse=True)
        else:
            return [fl for fl in self.response.body['exercicios'] if int(fl.get('id')) == int(filter)][0].get('programas')

    @view.memoize
    def exercisesDetail(self, code=None):
        """ """
        detail_excercise = self.apiCodevasf.exercise_detail.list()
        return [vl for vl in detail_excercise.body['exercicio_detalhe'] if vl.get('id') == code]

    def javascript(self):
        """ """
        return """
                (function($) {
                   var current_path = document.location.href;
                   var base_url = $("base").attr("href");

                   $("*[id^=programa_]").on('click', function(el){
                      $("." + this.id).toggle();
                   })
                })(jQuery);
               """

    def __call__(self):
        """"""
        self._setup()
        return self.template()
