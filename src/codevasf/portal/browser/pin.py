# -*- coding: utf-8 -*-
"""Default view for Pin content type."""
from Products.Five.browser import BrowserView

import cgi


class PinView(BrowserView):
    """Default view for Pin content type."""

    def text(self):
        return cgi.escape(getattr(self.context.text, 'raw', ''))

    def get_image_url(self, field='image', scale='preview'):
        """Return the URL of the object's image scale."""
        scales = self.context.restrictedTraverse('@@images')
        try:
            return scales.scale(field, scale=scale).url
        except AttributeError:
            return ''

    def get_file_url(self):
        """Return the URL of the object's image scale."""
        if self.context.listing is None:
            return ''

        filename = self.context.listing.filename
        return self.context.absolute_url() + '/@@download/listing/' + filename
