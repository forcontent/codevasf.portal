# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from codevasf.webservice.api import get_api_instance
from codevasf.portal.config import APISANDBOX

from plone import api
from six.moves.urllib import parse

from plone.memoize import view
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class RequireLoginView(BrowserView):
    template = ViewPageTemplateFile('templates/ws_login.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.isAnon = self.context.restrictedTraverse('@@plone_portal_state').anonymous()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    def __call__(self):
        """"""
        self._setup()
        url = '{}/ws_login'.format(self.context.absolute_url() or api.portal.get().absolute_url())
        came_from = self.form.get('came_from', None)
        sended = self.form.get('ws_login_send', None)
        login_cpf_cnpj = self.form.get('login_cpf_cnpj', None)

        if self.isAnon and sended:
            if not login_cpf_cnpj:
                if came_from:
                    url += '?came_from={0:s}'.format(parse.quote(came_from))
            else:
                    url = '{}/?searched={}&id={}&unidade={}'.format(came_from, self.form.get('searched',0), self.form.get('id',None), self.form.get('unidade',None))
            self.request.response.redirect(url)
        return self.template()


class RegisterView(BrowserView):
    template = ViewPageTemplateFile('templates/ws_register.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.isAnon = self.context.restrictedTraverse('@@plone_portal_state').anonymous()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    def __call__(self):
        """"""
        self._setup()
        url = '{0:s}/ws_register'.format(api.portal.get().absolute_url())
        came_from = self.form.get('came_from', None)
        sended = self.form.get('ws_login_send', None)
        login_cpf_cnpj = self.form.get('login_cpf_cnpj', None)

        if self.isAnon and sended:
            url = '{}/?searched={}&id={}&unidade={}'.format(came_from, self.form.get('searched',0), self.form.get('id',None), self.form.get('unidade',None))
            self.request.response.redirect(url)
        return self.template()
