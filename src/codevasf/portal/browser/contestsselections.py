# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from Acquisition import aq_parent
from codevasf.webservice.api import get_api_instance
from codevasf.portal.config import APISANDBOX

try:
    import simplejson as json
except:
    import json

from plone.memoize import view
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class ContestsSelectionsView(BrowserView):
    template = ViewPageTemplateFile('templates/contests_selections.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.response = self.apiCodevasf.contests_selections.list()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    @view.memoize
    def getContests(self):
        """ """
        fl = self.form.get('number_cpf')
        if fl:
            return self.response.body['concurso']

    def __call__(self):
        """"""
        self._setup()
        return self.template()
