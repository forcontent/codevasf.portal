# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from codevasf.webservice.api import get_api_instance
from codevasf.portal.config import APISANDBOX
from pyjsonq import JsonQ

try:
    # python2
    from urllib import urlencode
except ImportError:
    # python3
    from urllib.parse import urlencode

from plone.memoize import view
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class BiddingView(BrowserView):
    template = ViewPageTemplateFile('templates/bidding.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.response = self.apiCodevasf.bidding_filters.list()
        self.isAnon = self.context.restrictedTraverse('@@plone_portal_state').anonymous()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    @view.memoize
    def biddingsFilter(self):
        """ """
        return self.response.body['licitacoes_filtros']

    def getUnits(self, uf=None):
        """ """
        units = self.biddingsFilter()
        all_units = units.get('unidade')
        if uf:
            for dt in all_units:
                if uf in dt.get('id', ''):
                    return dt
        return all_units

    def getModality(self, modality=None):
        """ """
        filters = self.biddingsFilter()
        modalities = filters.get('modalidade', [])

        if modality:
            for ml in modalities:
                if modality in ml.get('id', ''):
                    modalities = ml.get('nome', '')
        return modalities

    def getType(self, tpe=None):
        """ """
        filters = self.biddingsFilter()
        types = filters.get('tipo', [])

        if tpe:
            for tp in types:
                if typ in tp.get('id', ''):
                    types = tp.get('tipo', '')
        return types

    def getStatus(self, st=None):
        """ """
        filters = self.biddingsFilter()
        status = filters.get('status', [])

        if st:
            for sts in status:
                if st in sts.get('id', ''):
                    status = sts.get('status', '')
        return status

    def getYears(self):
        """ """
        filters = self.biddingsFilter()
        return filters.get('ano', [])

    @view.memoize
    def getBiddings(self, not_burnish=None):
        """ """
        filters = self.apiCodevasf.biddings.list()

        if not_burnish:
            return filters
        if isinstance(self.request.get('unit'), dict):
            self.request.set('unit', self.form.get('unit', [])[0])
            self.form['unit'] = self.request.get('unit', [])
        return filters.body['licitacoes']

    def javascript(self):
        """ """
        return """
                (function($) {
                   var current_path = document.location.href;
                   var base_url = $("base").attr("href");
                   var form_filter = "form[name^=unit-filters]";
                   $( form_filter + '>select[name^=unit_code]').on('change', function(e){
                       e.preventDefault();
                       var selitem = $('select[name^=' + $(this).attr('name') + '] :selected').text();
                       $(form_filter + '>input[name^=unit]').attr({'value': selitem});
                       $(form_filter).submit();
                   })

                })(jQuery);
               """

    def __call__(self):
        """"""
        self._setup()
        return self.template()


class AdvancedSearch(BiddingView):
    template = ViewPageTemplateFile('templates/advsearch.pt')

    @view.memoize
    def getResults(self):
        """ """
        form = self.form
        rs = []

        if 'searched' in form:
            try:
                self.request.set('searched', form['searched'])
                try:
                    self.request.set('_authenticator', form['_authenticator'])
                except KeyError:
                    pass
                self.request.set('b_start', form.get('b_start', ''))
                self.request.set('detail', form.get('detail', ''))
                list_del = ['searched', '_authenticator',
                            'b_start', 'detail', 'req_rs']
                for dl in list_del:
                    if dl in form:
                        del form[dl]
            except KeyError:
                pass

            form_qe = [i.split('=') for i in urlencode(form).split('&') if i.split('=')[1] != '']
            qe = JsonQ(data=self.getBiddings(not_burnish=1).body)
            format_q = '.'.join(["where('{}', '=','{}')".format(i[0],i[1]) for i in form_qe])
            if format_q:
                rs = eval("qe.at('licitacoes').{}.get()".format(format_q))
        if rs:
            self.request.set('req_rs', 1)
            form['searched'] = self.request['searched']
            form['req_rs'] = self.request['req_rs']
            try:
                form['_authenticator'] = self.request['_authenticator']
            except KeyError:
                pass
            form['searched'] = self.request['searched']
        return rs
