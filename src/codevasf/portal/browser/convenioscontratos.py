# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from codevasf.webservice.api import get_api_instance
from codevasf.portal.config import APISANDBOX

try:
    import simplejson as json
except:
    import json

from plone.memoize import view
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class AgreementsContractsView(BrowserView):
    template = ViewPageTemplateFile('templates/agreements_contracts.pt')

    def _setup(self):
        """ """
        self.apiCodevasf = get_api_instance(sandbox=APISANDBOX)
        self.response = self.apiCodevasf.agreements_contracts.list()

        self.context = aq_inner(self.context)
        self.request = aq_inner(self.request)
        self.form = self.request.form

    @view.memoize
    def getAgreementscontracts(self):
        """ """
        return [{'id': agr['id'],
                 'value': agr['nome']} for agr in self.response.body['convenios']]

    @view.memoize
    def getFilters(self, id=1, fltype=None):
        """ """
        agreement_sel = self.response.body['convenios'][int(id - 1)]

        if not fltype:
            return [{'id': cont['id'],
                     'type': cont['tipo'],
                     'value': cont['nome']} for cont in agreement_sel['filtros']]
        else:
            return [{'id': cont['id'],
                     'type': cont['tipo'],
                     'value': cont['nome']} for cont in
                     agreement_sel['filtros'] if int(cont['id']) == int(fltype)]

    def javascript(self):
        """ """
        return """
                (function($) {
                   var current_path = document.location.href;
                   var base_url = $("base").attr("href");

                   var agreementsSel = '.agreements-contracts select[name=agreements_contracts]'
                   var filterSel = '.filters select[name=filter]'
                   var optValue = 'form[name=agreements_contracts] > input[name=optValue]'

                   if ($(optValue).val() == ""){
                      var covtext = $(agreementsSel + ' option:selected').text()
                      $(optValue).val(covtext)
                   }

                   $('.agreements-contracts select').on('change', function(e){
                      e.preventDefault();
                      var agreementCont = $(agreementsSel + ' option:selected').val();
                      var contUri = current_path + '?rsjson=1&filterid=' + agreementCont.split('_')[1];
                      console.log(contUri);

                      var filterOld = $(filterSel).html();
                      $(filterSel).html("");
                      $(optValue).val($(agreementsSel + ' option:selected').text())

                      $.getJSON(contUri , function(data) {
                         $.ajaxSetup({ cache: false}); // No cache
                         if (data != null) {
                            $.each(data, function(i, rs) {
                               $(filterSel).append($("<option>").text(rs.value).attr("value", rs.type));
                            });
                         }
                      }).fail(function( jqxhr, textStatus, error ) {
                         var err = textStatus + ", " + error;
                         console.log( "Request Failed: " + err );
                         $(filterSel).html(filterOld);
                      });
                   });

                   $("form[name=agreements_contracts]").submit(function(e){
                       //e.preventDefault(e);

                       var form = $("form[name=agreements_contracts]");
                       form.attr('action', $(filterSel).val());
                       return true;
                   });

                })(jQuery);
               """

    def __call__(self):
        """"""
        self._setup()
        data_json = json.dumps(self.getFilters(id=int(1)))

        if int(self.form.get('rsjson', 0)):
            filid = self.form.get('filterid')
            filtype = self.form.get('fltype')

            data_json = json.dumps(self.getFilters(id=int(filid)))

            return data_json
        else:
            return self.template()
        return self.template()
