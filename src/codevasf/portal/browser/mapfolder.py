# -*- coding: utf-8 -*-
"""Default view for MapFolder content type."""
from Products.Five.browser import BrowserView

import cgi


class MapFolderView(BrowserView):
    """Default view for MapFolder content type."""

    def get_pins(self):
        """Return a list of pins inside the map folder."""
        pins = []
        for p in self.context.listFolderContents():
            pins.append({
                'title': p.title,
                'description': p.description,
                'text': cgi.escape(getattr(p.text, 'raw', '')),
                'latitude': p.latitude,
                'longitude': p.longitude,
                'image': self.get_image_url(p),
                'caption': p.image_caption or '',
                'mapping': self.get_image_url(p, 'mapping'),
                'listing': self.get_file_url(p),
                'url': p.absolute_url(),
            })
        return pins

    def get_image_url(self, obj, field='image', scale='preview'):
        """Return the URL of the object's image scale."""
        scales = obj.restrictedTraverse('@@images')
        try:
            return scales.scale(field, scale=scale).url
        except AttributeError:
            return ''

    def get_file_url(self, obj):
        """Return the URL of the object's image scale."""
        if obj.listing is None:
            return ''

        filename = obj.listing.filename
        return obj.absolute_url() + '/@@download/listing/' + filename
