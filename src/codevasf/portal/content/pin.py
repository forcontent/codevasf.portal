# -*- coding: utf-8 -*-
"""The Pin content type."""
from codevasf.portal import _
from plone.app.dexterity import PloneMessageFactory as _PMF
from plone.app.textfield import RichText
from plone.app.textfield.interfaces import ITransformer
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.indexer import indexer
from plone.namedfile.field import NamedBlobFile
from plone.namedfile.field import NamedBlobImage
from plone.supermodel import model
from Products.CMFPlone.utils import safe_unicode
from z3c.form.interfaces import IAddForm
from z3c.form.interfaces import IEditForm
from zope import schema
from zope.interface import implementer


class IPin(model.Schema):
    """A Pin."""

    title = schema.TextLine(
        title=_(u'label_title', default=u'Name'),
        required=True,
    )

    description = schema.Text(
        title=_PMF(u'label_description', default=u'Summary'),
        description=_PMF(
            u'help_description',
            default=u'Used in item listings and search results.',
        ),
        required=False,
        missing_value=u'',
    )

    text = RichText(
        title=_(u'Body text'),
        required=False,
    )

    latitude = schema.ASCIILine(
        title=_(u'label_latitude', default=u'Latitude'),
        description=_(
            u'help_latitude',
            default=u"Geographic coordinate that specifies the north–south position of a point on the Earth's surface.",  # noqa: E501
        ),
        required=True,
        default='',
    )

    longitude = schema.ASCIILine(
        title=_(u'label_longitude', default=u'Longitude'),
        description=_(
            u'help_longitude',
            default=u"Geographic coordinate that specifies the east-west position of a point on the Earth's surface.",  # noqa: E501
        ),
        required=True,
        default='',
    )

    mapping = NamedBlobImage(
        title=_(u'label_mapping', default=u'Mapping'),
        description=_(
            u'help_mapping',
            default=u'',
        ),
        required=False,
    )

    listing = NamedBlobFile(
        title=_(u'label_listing', default=u'Listing'),
        description=_(
            u'help_listing',
            default=u'',
        ),
        required=False,
    )

    directives.omitted('title', 'description')
    directives.no_omit(IEditForm, 'title', 'description')
    directives.no_omit(IAddForm, 'title', 'description')


@implementer(IPin)
class Pin(Item):
    """A Pin."""


@indexer(IPin)
def textIndexer(obj):
    """SearchableText contains id, title and abstract."""
    transformer = ITransformer(obj)

    try:
        text = transformer(obj.text, 'text/plain')
    except AttributeError:
        text = ''

    return u' '.join((
        safe_unicode(obj.id),
        safe_unicode(obj.title) or u'',
        safe_unicode(obj.description) or u'',
        safe_unicode(text),
    ))
