# -*- coding: utf-8 -*-
"""The MapFolder content type."""
from codevasf.portal import _
from plone.app.textfield import RichText
from plone.dexterity.content import Container
from plone.supermodel import model
from zope.interface import implementer


class IMapFolder(model.Schema):
    """A folder to store counties."""

    text = RichText(
        title=_(u'Body text'),
        required=False,
    )


@implementer(IMapFolder)
class MapFolder(Container):
    """A folder to store counties."""
