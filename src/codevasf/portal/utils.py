# -*- coding: utf-8 -*-


def human_readable_size(size):
    """Return human readable size.
    Based on https://stackoverflow.com/a/1094933
    """
    if size < 1024:
        return '{size} B'.format(size=size)

    for unit in ('KB', 'MB', 'GB'):
        size /= 1024.0
        if size < 1024.0:
            return '{size:.1f} {unit}'.format(size=size, unit=unit)

    return '{size:.1f} GB'.format(size=size)
