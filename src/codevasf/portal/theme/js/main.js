$(function() {
  // Tabs tile
  var changeItem = function($tile, item) {
    var $tabItem = $('.tab-item[data-kind="'+item+'"]', $tile);
    $('.tab-item', $tile).removeClass('active');
    $tabItem.addClass('active');
  };
  $('.tab-button a').on('click', function(e) {
    e.preventDefault();
    var $tile = $(this).parents('.tabs-tile');
    var $tabButton = $(this).parents('.tab-button');
    if ($tabButton.hasClass('active')) {
      return;
    }
    $('.tab-button', $tile).removeClass('active');
    $tabButton.addClass('active');
    changeItem($tile, $tabButton.attr('data-kind'));
  });
  $('.tab-dropdown').on('change', function(e) {
    e.preventDefault();
    var $tile = $(this).parents('.tabs-tile');
    var $selection = $(this);
    changeItem($tile, $selection.val());
  });

  // Two selects tile
  $('.twoselects-tile .first-select').on('change', function(e) {
    e.preventDefault();
    var $tile = $(this).parents('.twoselects-tile');
    var firstSelection = $(this).val();
    $('.twoselects-results > div', $tile).removeClass('active');
    $('.second-select select', $tile).removeClass('active');
    if (firstSelection === '') {
      $('.second-select select:first()', $tile).addClass('active');
      return;
    }
    var $secondSelect = $('.second-select select[data-folder="'+firstSelection+'"]', $tile);
    $secondSelect.addClass('active');
  });
  $('.twoselects-tile .second-select select').on('change', function(e) {
    e.preventDefault();
    var $tile = $(this).parents('.twoselects-tile');
    var firstSelection = $('.first-select', $tile).val();
    var secondSelection = $(this).val();
    $('.twoselects-results > div', $tile).removeClass('active');
    if (secondSelection === '') {
      return;
    }
    var $page = $('.twoselects-results div[data-folder="'+firstSelection+'"][data-page="'+secondSelection+'"]', $tile);
    $page.addClass('active');
  });
  $('.goback').on('click', function(e){
      e.preventDefault();
      window.history.back();
  });

  $('.register_sendform').prepOverlay({
      subtype:'ajax',
      filter: '#register_modal > *'
  });


});
