# -*- coding: utf-8 -*-
from codevasf.portal import _
from codevasf.portal.widgets.tabs import TabsFieldWidget
from collective.cover.interfaces import ITileEditForm
from collective.cover.tiles.configuration_view import IDefaultConfigureForm
from collective.cover.tiles.list import IListTile
from collective.cover.tiles.list import ListTile
from collective.cover.utils import get_types_use_view_action_in_listings
from plone.app.textfield import RichText
from plone.autoform import directives as form
from plone.tiles.interfaces import ITileDataManager
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary


class ITabsTile(IListTile):

    form.no_omit(IDefaultConfigureForm, 'uuids')
    form.no_omit(ITileEditForm, 'uuids')
    form.widget(uuids=TabsFieldWidget)
    form.order_after(uuids='*')

    form.omitted('text')
    form.no_omit(IDefaultConfigureForm, 'text')
    text = RichText(title=u'Text')

    form.omitted('selection_type')
    form.no_omit(ITileEditForm, 'selection_type')
    form.widget(selection_type=RadioFieldWidget)
    selection_type = schema.Choice(
        title=_(u'Selection Type'),
        vocabulary=SimpleVocabulary([
            SimpleTerm(value=u'tabs', title=_(u'Tabs')),
            SimpleTerm(value=u'dropdown', title=_(u'Dropdown')),
        ]),
        required=True,
        default=u'tabs',
    )

    form.omitted('selection_label')
    form.no_omit(ITileEditForm, 'selection_label')
    selection_label = schema.TextLine(
        title=_(u'Selection Label'),
        description=_(u'This field is just used when select "Dropdown" option.'),
        required=False,
        default=_(u'Select and item to show:'),
    )


@implementer(ITabsTile)
class TabsTile(ListTile):

    index = ViewPageTemplateFile('tabs.pt')

    is_configurable = True
    is_droppable = True
    is_editable = True
    short_name = _(u'msg_short_name_tabs', default=u'Tabs')
    limit = 5

    def show_buttons(self):
        return len(self.results()) > 1

    def get_title(self, item):
        """Get the title of the item, or the custom title if set.

        :param item: [required] The item for which we want the title
        :type item: Content object
        :returns: the item title
        :rtype: unicode
        """
        # First we get the title for the item itself
        title = item.Title()
        uuid = self.get_uuid(item)
        data_mgr = ITileDataManager(self)
        data = data_mgr.get()
        uuids = data['uuids']
        if uuid in uuids:
            if uuids[uuid].get('custom_title', u''):
                # If we had a custom title set, then get that
                title = uuids[uuid].get('custom_title')
        return title

    def get_description(self, item):
        """Get the description of the item, or the custom description
        if set.

        :param item: [required] The item for which we want the description
        :type item: Content object
        :returns: the item description
        :rtype: unicode
        """
        # First we get the url for the item itself
        description = item.Description()
        uuid = self.get_uuid(item)
        data_mgr = ITileDataManager(self)
        data = data_mgr.get()
        uuids = data['uuids']
        if uuid in uuids:
            if uuids[uuid].get('custom_description', u''):
                # If we had a custom description set, then get that
                description = uuids[uuid].get('custom_description')
        return description

    def get_url(self, item):
        """Get the URL of the item, or the custom URL if set.

        :param item: [required] The item for which we want the URL
        :type item: Content object
        :returns: the item URL
        :rtype: str
        """
        # First we get the url for the item itself
        url = item.absolute_url()
        if item.portal_type in get_types_use_view_action_in_listings():
            url += '/view'
        uuid = self.get_uuid(item)
        data_mgr = ITileDataManager(self)
        data = data_mgr.get()
        uuids = data['uuids']
        if uuid in uuids:
            if uuids[uuid].get('custom_url', u''):
                # If we had a custom url set, then get that
                url = uuids[uuid].get('custom_url')
        return url

    def _get_title_tag(self, item):
        """Return the HTML code used for the title as configured on the tile.

        :param item: [required]
        :type item: content object
        """
        tag = '<{heading}><a href="{href}">{title}</a></{heading}>'
        if self._field_is_visible('title'):
            tile_conf = self.get_tile_configuration()
            title_conf = tile_conf.get('title', None)
            if title_conf:
                heading = title_conf.get('htmltag', 'h2')
                href = self.get_url(item)
                title = self.get_title(item)
                return tag.format(heading=heading, href=href, title=title)

    def get_text(self, item):
        """Get the text of the item, or the custom text if set.

        :param item: [required] The item for which we want the text
        :type item: Content object
        :returns: the item text
        :rtype: unicode
        """
        # First we get the text for the item itself
        text = ''
        try:
            text = item.getText()  # Archetypes
        except AttributeError:
            if item.text:
                text = item.text.output  # Dexterity
        uuid = self.get_uuid(item)
        data_mgr = ITileDataManager(self)
        data = data_mgr.get()
        uuids = data['uuids']
        if uuid in uuids:
            if uuids[uuid].get('custom_text', u''):
                # If we had a custom text set, then get that
                text = uuids[uuid].get('custom_text')
        return text

    @property
    def show_tabs(self):
        return (self.data['selection_type'] == u'tabs')

    @property
    def selection_label(self):
        return self.data['selection_label']
