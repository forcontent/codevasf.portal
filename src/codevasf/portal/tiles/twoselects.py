# -*- coding: utf-8 -*-
from codevasf.portal import _
from collective.cover.interfaces import ITileEditForm
from collective.cover.tiles.base import IPersistentCoverTile
from collective.cover.tiles.base import PersistentCoverTile
from plone import api
from plone.app.uuid.utils import uuidToObject
from plone.autoform import directives as form
from plone.tiles.interfaces import ITileDataManager
from plone.uuid.interfaces import IUUID
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope import schema
from zope.interface import implementer


class ITwoSelectsTile(IPersistentCoverTile):

    form.omitted('first_select_label')
    form.no_omit(ITileEditForm, 'first_select_label')
    first_select_label = schema.TextLine(
        title=_(u'First Select Label'),
        required=False,
        default=_(u'Select the unit...'),
    )

    form.omitted('second_select_label')
    form.no_omit(ITileEditForm, 'second_select_label')
    second_select_label = schema.TextLine(
        title=_(u'Second Select Label'),
        required=False,
        default=_(u'And the period...'),
    )


@implementer(ITwoSelectsTile)
class TwoSelectsTile(PersistentCoverTile):

    index = ViewPageTemplateFile('twoselects.pt')

    is_configurable = False
    is_droppable = True
    is_editable = True
    short_name = _(u'msg_short_name_twoselects', default=u'Two Selects')

    def results(self):
        uuid = self.data.get('uuid', None)
        obj = uuidToObject(uuid)
        folders = api.content.find(
            context=obj,
            depth=1,
            portal_type='Folder',
            sort_on='getObjPositionInParent',
        )
        data = []
        for brain in folders:
            folder = brain.getObject()
            pages = api.content.find(
                context=folder,
                depth=1,
                sort_on='getObjPositionInParent',
            )
            data.append({
                'id': brain.id,
                'title': brain.Title,
                'pages': [{
                    'id': page.id,
                    'title': page.Title,
                } for page in pages]})
        return data

    def is_empty(self):
        return self.results() == []

    def accepted_ct(self):
        """Return Folder as the only content type accepted in the tile.
        """
        return ['Folder']

    def populate_with_object(self, obj):
        super(TwoSelectsTile, self).populate_with_object(obj)  # check permission
        if obj.portal_type not in self.accepted_ct():
            return
        data_mgr = ITileDataManager(self)
        data_mgr.set({'uuid': IUUID(obj)})  # noqa: P001

    @property
    def first_select_label(self):
        return self.data['first_select_label']

    @property
    def second_select_label(self):
        return self.data['second_select_label']

    def get_page(self, folder_id, page_id):
        uuid = self.data.get('uuid', None)
        obj = uuidToObject(uuid)
        if not obj:
            return
        folder = obj.get(folder_id)
        page = folder.get(page_id)
        text = page.text
        if not text:
            return
        return text.output
