# -*- coding: utf-8 -*-
from Acquisition import ImplicitAcquisitionWrapper
from collective.cover.utils import get_types_use_view_action_in_listings
from collective.cover.utils import uuidToObject
from collective.cover.widgets.interfaces import ITextLinesSortableWidget
from collective.cover.widgets.textlinessortable import TextLinesSortableWidget
from plone.app.z3cform.utils import closest_content
from Products.CMFPlone.utils import safe_unicode
from UserDict import UserDict
from z3c.form import interfaces
from z3c.form import widget
from zope.browserpage.viewpagetemplatefile import ViewPageTemplateFile

import zope.interface


class ITabsWidget(ITextLinesSortableWidget):
    """Tabs widget."""


class TabsWidget(TextLinesSortableWidget):
    """ Widget to edit tabs item."""

    zope.interface.implementsOnly(ITabsWidget)
    klass = u'tabs-widget'
    configure_template = ViewPageTemplateFile('tabs_configure.pt')
    display_template = ViewPageTemplateFile('tabs_display.pt')
    input_template = ViewPageTemplateFile('tabs_input.pt')

    def wrapped_context(self, obj):
        context = obj
        content = closest_content(context)
        # We'll wrap context in the current site *if* it's not already
        # wrapped.  This allows the template to acquire tools with
        # ``context/portal_this`` if context is not wrapped already.
        # Any attempts to satisfy the Kupu template in a less idiotic
        # way failed. Also we turn dicts into UserDicts to avoid
        # short-circuiting path traversal. :-s
        if context.__class__ == dict:
            context = UserDict(self.context)
        return ImplicitAcquisitionWrapper(context, content)

    def get_custom_text(self, uuid):
        """ Returns the custom Text assigned to a specific item

        :param uuid: [required] The object's UUID
        :type uuid: string
        :returns: The custom Description
        """
        # Try to get custom description
        text = u''
        uuids = self.context['uuids']
        values = [uuids[i] for i in uuids if i == uuid]
        if values:
            text = values[0].get('custom_text', u'')
        if text:
            return text
        # If didn't find, get object text
        obj = uuidToObject(uuid)
        try:
            return safe_unicode(obj.getText())  # Archetypes
        except AttributeError:
            if obj.text is None:
                return u''
            return safe_unicode(obj.text.output)  # Dexterity

    def extract(self):
        """ Extracts the data from the HTML form and returns it

        :returns: A dictionary with the information
        """
        values = []
        if self.name in self.request:
            values = self.request.get(self.name).splitlines()
        uuids = [i for i in values if i]
        results = dict()
        for index, uuid in enumerate(uuids):
            obj = uuidToObject(uuid)
            results[uuid] = {
                u'order': unicode(index),
            }
            custom_title = self.request.get(
                '{0}.custom_title.{1}'.format(self.name, uuid), '',
            )
            if (custom_title != u'' and
               custom_title != safe_unicode(obj.Title())):
                results[uuid][u'custom_title'] = unicode(custom_title)
            custom_description = self.request.get(
                '{0}.custom_description.{1}'.format(self.name, uuid), '',
            )
            if (custom_description != u'' and
               custom_description != safe_unicode(obj.Description())):
                results[uuid][u'custom_description'] = unicode(custom_description)
            custom_text = self.request.get(
                '{0}.custom_text.{1}'.format(self.name, uuid), '',
            )
            text = getattr(obj, 'text', None)
            if (custom_text != u'' and
               text is not None and
               custom_text != safe_unicode(text)):
                results[uuid][u'custom_text'] = unicode(custom_text)
            custom_url = self.request.get(
                '{0}.custom_url.{1}'.format(self.name, uuid), '')
            url = obj.absolute_url()
            if obj.portal_type in get_types_use_view_action_in_listings():
                url += '/view'
            if (custom_url != u'' and
               custom_url != url):
                results[uuid][u'custom_url'] = unicode(custom_url)
        return results


@zope.interface.implementer(interfaces.IFieldWidget)
def TabsFieldWidget(field, request):
    """IFieldWidget factory for TabsWidget."""
    return widget.FieldWidget(field, TabsWidget(request))
